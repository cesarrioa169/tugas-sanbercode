<?php

use App\Http\Controllers\CRUDController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'index'])->name('register');
Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');

Route::get('/tabel', function(){
    return view('tabel.tabel');
});

Route::get('/data-tabel', function(){
    return view('tabel.data-tabel');
});

//insert database
Route::get('/cast/create', [CRUDController::class, 'create']);
Route::post('/cast', [CRUDController::class, 'store']);

//tampil data
Route::get('/tampildata', [CRUDController::class, 'tampildata']);

//tampil detail data
Route::get('/tampildata/{cast}', [CRUDController::class, 'show']);

//edit data
Route::get('/editdata/{cast}', [CRUDController::class, 'edit']);

//update
Route::put('/update/{cast}', [CRUDController::class, 'update']);

//detele data
Route::delete('/hapus/{cast}', [CRUDController::class, 'destroy']);

//CRUD Film
Route::resource('film', FilmController::class);





