@extends('page.master')

@section('title')
CREATE
@endsection

@section('title2')
untuk CREATE FILM
@endsection

@section('content')

<a href="film/create" class="btn btn-primary my-3">Tambah Film</a>

<div class="container">
    <div class="row">
        @forelse ($film as $item)


        <div class="col-sm">
              
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src={{asset('image/'.$item->poster)}} alt="Card image cap" height="300">
                <div class="card-body">
                  <h1>{{$item->judul}}</h1>
                  <p class="card-text">{{Str::limit($item->ringkasan , 100, ' ...')}}</p>
                  <a href="#" class="btn btn-primary">Read More</a>
                </div>
              </div>
        
        
          </div>
          
        @empty
            
        @endforelse
        
    </div>
  </div>



@endsection