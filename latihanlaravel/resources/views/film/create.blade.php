@extends('page.master')

@section('title')
CREATE
@endsection

@section('title2')
untuk CREATE FILM
@endsection

@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input name="judul" type="text" class="form-control">
    </div>

    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Ringkasan</label>
        <textarea name="ringkasan" class="form-control" rows="3"></textarea>
    </div>

    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <select name="genre" class="custom-select custom-select-lg mb-3">
        <option value="">Silahkan Pilih Genre kamu</option>
        @forelse ($genre as $item)
        <option value={{$item->id}}>{{$item->nama}}</option>
        
        @empty
            
        @endforelse
       
    </select>

    @error('genre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <br>

    <div class="custom-file">
        <input name="poster" type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="customFile">Pilih Gambar</label>
    </div>

    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Tahun</label>
        <input name="tahun" type="number" class="form-control">
    </div>

    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection

@push('namafileupload')
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

</script>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

@endpush
