@extends('page.master')

@section('title')
Tampil Data
@endsection

@section('title2')
untuk Tampil Data <br>
<a href="/cast/create" class="btn btn-info">DATA BARU</a>
@endsection

@section('content')

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$item)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>


                <form action="/hapus/{{$item->id}}" method="post">

                    <a href="/tampildata/{{$item->id}}" class="btn btn-info">Show</a>
                    <a href="/editdata/{{$item->id}}" class="btn btn-warning">edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" value="DELETE" class="btn btn-danger">

                </form>



            </td>
        </tr>
        @empty

        @endforelse
    </tbody>
</table>

@endsection
