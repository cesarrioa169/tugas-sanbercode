@extends('page.master')

@section('title')
edit
@endsection

@section('title2')
untuk edit
@endsection

@section('content')

<form action="/update/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input name="nama" value="{{$cast->nama}}" type="text" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea name="bio" class="form-control" rows="3">{{$cast->bio}}</textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input name="umur" value="{{$cast->umur}}" type="number" class="form-control">
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
