@extends('page.master')

@section('title')
CREATE
@endsection

@section('title2')
untuk CREATE
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input name="nama" type="text" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea name="bio" class="form-control" rows="3"></textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input name="umur" type="number" class="form-control">
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
