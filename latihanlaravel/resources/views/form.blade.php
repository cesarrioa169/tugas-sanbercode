@extends('page.master');

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action={{ route('welcome') }} method="post">
    @csrf
    <label>First name:</label><br />
    <input type="text" name="first_name" required /><br /><br />
    <label>Last name:</label><br />
    <input type="text" name="last_name" required /><br /><br />
    <label>Gender:</label><br />
    <input type="radio" name="gender" />Male<br />
    <input type="radio" name="gender" />Female<br /><br />
    <label>Nationality:</label><br />
    <select name="nationality">
        <option value="Indonesian" selected>Indonesian</option>
        <option value="Singapura" disabled>Singapura</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Vietnam">Vietnam</option>
    </select><br /><br />
    <label>Language Spoken:</label><br />
    <input type="checkbox" name="language_spoken">Bahasa Indonesia<br />
    <input type="checkbox" name="language_spoken">English<br />
    <input type="checkbox" name="language_spoken">Other<br /><br />
    <label>Bio:</label><br />
    <textarea name="bio" cols="30" rows="10"></textarea><br />
    <input type="submit" value="Sign Up" />
</form>
@endsection
