<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class CRUDController extends Controller
{
    public function create()
    {
        return view ('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'required',
            'umur.required' => 'required',
            'bio.required' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect ('/tampildata');
        // $query = DB::table('post')->insert([
        //     "title" => $request["title"],
        //     "body" => $request["body"]
        // ]);
        // return redirect('/posts');
    }

  public function tampildata()
  {
    $data = DB::table('cast')->get();

    return view ('post.tampildata',['data'=>$data]);
  }

  
public function show($id)
{
    $cast = DB::table('cast')->find($id);
    return view('post.show', ['cast' => $cast]);
}

public function edit ($id)
{
    $cast = DB::table('cast')->find($id);
    return view('post.edit', ['cast' => $cast]);

}

public function update ($id, Request $request)
{
    $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
    ],
    [
        'nama.required' => 'required',
        'umur.required' => 'required',
        'bio.required' => 'required',
    ]);

    DB::table('cast')
    ->where('id',$id)
    ->update([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'],
    ]);

    return redirect ('/tampildata');

}


public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/tampildata');
    }

    
}
