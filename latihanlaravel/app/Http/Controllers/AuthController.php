<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('form');
    }
    public function welcome(Request $request)
    {
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name
        ];
        return view('welcome', $data);
    }
}
